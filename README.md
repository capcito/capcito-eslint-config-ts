# @capcito/eslint-config-ts
This package is a contains eslint configuration for all TypeScript based packages.

## Deployment
This is a utility package, meaning you won't deploy it by itself, it'll get bundled with your other application if you're depending on functionallity from this package.